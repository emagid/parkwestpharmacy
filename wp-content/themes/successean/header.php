<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700|Fanwood+Text:400,400italic' rel='stylesheet' type='text/css'>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="site-wrapper">

			<!-- header -->
			<header class="site-header clear" role="banner">
				<div class="header-wrapper">

					<!-- nav -->
					<nav class="site-nav" role="navigation">
						<ul class="nav-left">
							<li class="home-link">
								<a href="<?php echo home_url(); ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/home-button.svg" alt="Home Button" class="home-button">
								</a>
							</li>
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu-left' ) ); ?>
						</ul>
						<ul class="nav-right">
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu-right' ) ); ?>
							<li class="nav-button">
								<a href="/refills">
									<button class="refill-button">Refill Your Prescription</button>
								</a>
							</li>
							
						</ul>
					</nav>
					<!-- /nav -->

				</div>

				<!-- logo -->
				<div class="logo">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/pwp-main-logo.png" alt="Logo" class="logo-img">
					</a>
				</div>
				<!-- /logo -->

			</header>
			<!-- /header -->
