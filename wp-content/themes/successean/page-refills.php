<?php get_header(); ?>
<div class="contact-wrapper">
	<div class="refill-section">
		<div class="contact-section-form refill-form">
			<h2>Prescription Refills</h2> 
			<h3>Please fill in all the information below for up to five prescription refill requests. You will be be contacted with the status of your request.</h3>
			<?php echo do_shortcode('[contact-form-7 id="80" title="Refill Prescription Form"]'); ?>
		</div>
	</div>

</div>

<?php get_footer(); ?>