
	<!-- footer -->
			<footer class="site-footer" role="contentinfo" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/pwp-footer-logo.png" alt="Footer Logo" class="footer-logo");>

				<div class="footer-wrapper">
					<div class="footer-left">
						<div class="dotted-border">
							<div class="footer-newsletter">
								<?php echo do_shortcode('[wysija_form id="1"]'); ?>
							</div>
						</div>

						<div class="footer-contact-info">
							<h4>Contact Us</h4>
							<p>463 Columbus Ave.</p>
							<p>Tel: 212-721-3883</p>
							<p>Fax: 212-721-5660</p>
						</div>
					</div>

					<div class="footer-right">
						<div class="footer-social">
							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-fb.png" alt="Facebook">
							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-twitter.png" alt="Twitter">
							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-ig.png" alt="Instagram">
							<img src="<?php echo get_template_directory_uri(); ?>/img/footer-mail.png" alt="Email">
							<p class="contact-num">212 721 3883</p>
						</div>
						<div class="footer-links">
							
								
									<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-left' ) ); ?>
								
							
							
								
									<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-right' ) ); ?>
								
							
						</div>
					</div>
				</div>

			</footer>
			<!-- /footer -->

		
		

		<?php wp_footer(); ?>

		<!-- analytics -->

	</body>
</html>
