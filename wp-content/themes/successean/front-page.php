<?php get_header(); ?>

<div class="front-section-uno">
	<a href="/services">
		<img src="<?php echo get_template_directory_uri(); ?>/img/banners/pwp_1.jpg" alt="Home Banner">
	</a>
</div>

<div class="front-section-dos" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/nyc-skyline.jpg" alt="NYC Skyline");>
	<div class="fsd-text-box">
		<h3>Our Story</h3>
		<div class="skinny-border"></div>
		<p><?php the_field('our_story'); ?></p>

		<h4>
			<a class="tan-text-link" href="/services">Find Out More</a>
		</h4>
	</div>
</div>

<div class="front-section-tres">
	<div class="fst-wrapper">

	<div class="fst-grid">
		<div class="fst-grid-top">
			<div class="fst-header-1 fst-header">
				<h2>Bath & Body</h2>
			</div>
			<div class="fst-header-2 fst-header">
				<h2>Health & Nutrition</h2>
			</div>
			<div class="fst-header-3 fst-header">
				<h2>Mothering & Baby</h2>
			</div>
		</div>

		<div class="fst-grid-bottom">
			<div class="grid-bot-layer-1">
				<div class="grid-bot-block grid-bot-block-1">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pwp_36.jpg" alt="Item">
				</div>
                				<div class="grid-bot-block grid-bot-block-1">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pwp_69.jpg" alt="Item">
				</div>
                				<div class="grid-bot-block grid-bot-block-1">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pwp_74.jpg" alt="Item">
				</div>
                				<div class="grid-bot-block grid-bot-block-1">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pwp_73.jpg" alt="Item">
				</div>
<!--
				<div class="grid-bot-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pwp_69.jpg" alt="Item">
				</div>
-->
<!--
				<div class="grid-bot-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/item-3.jpg" alt="Item">
				</div>
			</div>
			<div class="grid-bot-layer-2">
				<div class="grid-bot-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/item-4.jpg" alt="Item">
				</div>
				<div class="grid-bot-block">
					<img src="<?php echo get_template_directory_uri(); ?>/img/item-5.jpg" alt="Item">
				</div>
-->
			</div>
		</div>
	</div>
</div>

</div>

<?php get_footer(); ?>
