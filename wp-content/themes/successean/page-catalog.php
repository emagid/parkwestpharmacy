<?php get_header(); ?>
<div class="about-wrapper">
	<div class="about-intro">
		<div class="catalog-header">
			<h2>Check Out Our Catalog of Fine Products....</h2>
		</div>

		<div class="catalog-links-section">
			<div class="catalog-links">
				<a href="">Bath & Body</a>
				<a href="">Health & Nutrition</a>
				<a href="" class="active">Mothering & Baby</a>
			</div>	
		</div>

		<div class="catalog-view">
			<?php echo do_shortcode("[huge_it_catalog id='8']"); ?>
		</div>
	</div>

	</div>

	

<?php get_footer(); ?>

<script>
$( ".servy-scroll" ).scrollTop( 300 );
</script>