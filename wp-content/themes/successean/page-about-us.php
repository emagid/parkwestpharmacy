<?php get_header(); ?>
<div class="about-wrapper">
	<div class="about-intro">
		<div class="about-header">
			<h2>Values we've held onto for <span class="bold">35</span> Years</h2>
		</div>

		<div class="ring-section">
			<div class="about-ring quality-ring">
				<h4>Quality</h4>
			</div>

			<div class="outer-ring">
				<div class="about-ring honesty-ring active">		
					<h4>Honesty</h4>
				</div>
			</div>

			<div class="about-ring local-ring">
				<h4>Local</h4>
			</div>
		</div>

		<div class="about-intro-text" id="quality-text" style="display:none;">
			<p><?php the_field('quality_text'); ?></p>
		</div>

		<div class="about-intro-text" id="honesty-text">
			<p><?php the_field('honesty_text'); ?></p>
		</div>

		<div class="about-intro-text" id="local-text" style="display:none;">
			<p><?php the_field('local_text'); ?></p>
		</div>
	</div>

	<div class="about-links-section">
		<div class="about-links">
			<a href="" class="active">Our Values</a>
			<a href="">Our Team</a>
			<a href="">Our Story</a>
		</div>	
	</div>

	<div class="value-section" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/pwp_30.jpg" alt="City Sunset");>
		<div class="value-wrapper">

			<div class="value-side">

<!--
			<//?php
	  			$args = array(
	    		'post_type' => 'our_values'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
-->

<!--
				<a class="value-person" data-rel="person<?php the_field('id'); ?>">
					<img src="<?php the_field('image'); ?>" class="active" alt="Value Person">
				</a>
-->

				
<!--
		<//?php
			}
				}
			else {
			echo 'Content Not Found';
			}
		?>
-->

			</div>

		<div class="value-controller">
			<div class="value-main-clone" id="about">

				<h3>Robert Mathis, Pharmacist</h3>
				<h2><?php the_field('quote'); ?></h2>
				<div class="skinny-border"></div>
				<p><?php the_field('quote_in_depth'); ?></p>

			</div>

<!--
			<//?php
	  			$args = array(
	    		'post_type' => 'our_values'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
-->


			<div class="value-main" id="person<?php the_field('id'); ?>" style="display:none;">

			

				<h3><?php the_field('name'); ?>, <?php the_field('position'); ?></h3>
				<h2><?php the_field('title'); ?></h2>
				<div class="skinny-border"></div>
				<p><?php the_field('short_summary'); ?></p>

			</div>

<!--
		<//?php
			}
				}
			else {
			echo 'Content Not Found';
			}
		?>
-->

			
	</div>
		</div>
	</div>

	<div class="about-links-section">
		<div class="about-links">
			<a href="">Our Values</a>
			<a href="" class="active">Our Team</a>
			<a href="">Our Story</a>
		</div>	
	</div>

	<div class="our-story-section" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/pwp_35.jpg" alt="City Sunset");>
		<div class="our-story-backdrop">
			<div class="our-story-links">
				<a href="#" id="1987_link" class="active">1987</a>
				<a href="#" id="1993_link">1993</a>
				<a href="#" id="2012_link">2012</a>
				<a href="#" id="2016_link">2016</a>
			</div>	
			<div class="our-story-text-box">
				<h3>Our Story</h3>
				<div class="skinny-border"></div>
				<p id="1987_text" style="display:block;">Park West officially makes plans to move into the neighborhood when we choose our Upper West Side pharmacy location and begin construction on the storefront that customers visit today.</p>
                
                <p id="1993_text">Getting to know our customers has given us great feedback about the services and products our neighbors need. We expand our product offerings to include many of the brands that continue to be bestsellers.</p>
                
                <p id="2012_text">Technology improvements help Park West Pharmacy continue to improve upon our commitment to customer service and we can offer the convenience of placing requests online for prescription refills.</p>
                
                <p id="2016_text">You can now find us everywhere! In addition to launching a new website, Park West is now on social media, including: Facebook, Twitter and Instagram.</p>
			</div>
		</div>
	</div>

	<div class="about-links-section">
		<div class="about-links">
			<a href="">Our Values</a>
			<a href="">Our Team</a>
			<a href="" class="active">Our Story</a>
		</div>	
	</div>

<?php get_footer(); ?>

<script>
$( ".servy-scroll" ).scrollTop( 300 );
</script>
    
    <script type="text/javascript">
	jQuery('#1987_link').click(function(e) {
        e.preventDefault();
		jQuery('.our-story-backdrop a').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.our-story-text-box p').hide();
        jQuery('#1987_text').show();
	});

	jQuery('#1993_link').click(function(e) {
        e.preventDefault();
		jQuery('.our-story-backdrop a').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.our-story-text-box p').hide();
        jQuery('#1993_text').show();
	});

	jQuery('#2012_link').click(function(e) {
        e.preventDefault();
		jQuery('.our-story-backdrop a').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.our-story-text-box p').hide();
        jQuery('#2012_text').show();
	});
        
    jQuery('#2016_link').click(function(e) {
        e.preventDefault();
		jQuery('.our-story-backdrop a').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.our-story-text-box p').hide();
        jQuery('#2016_text').show();
	});

</script>

<script type="text/javascript">
	jQuery('.quality-ring').click(function() {
		jQuery('.about-ring').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('#quality-text').show();
		jQuery('#honesty-text').hide();
		jQuery('#local-text').hide();
	});

	jQuery('.honesty-ring').click(function() {
		jQuery('.about-ring').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('#honesty-text').show();
		jQuery('#quality-text').hide();
		jQuery('#local-text').hide();
	});

	jQuery('.local-ring').click(function() {
		jQuery('.about-ring').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('#local-text').show();
		jQuery('#honesty-text').hide();
		jQuery('#quality-text').hide();
	});

</script>

<script type="text/javascript">
//jQuery(".value-person").hover(function(e) {
//    e.preventDefault();
//    jQuery('.value-controller div').hide();
//    jQuery('#' + jQuery(this).data('rel')).show();
//});
</script>